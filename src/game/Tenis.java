package game;
import game.base.AbstractGame;

import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import sprite.Ball;
import sprite.Racquet;


public class Tenis extends AbstractGame{

	private static final long serialVersionUID = -7803629994015778818L;

	private final Ball ball = new Ball(this, 30, 0);
	private final Racquet racquet = new Racquet(this);

	private int speed = 1;
	private int score = 0;

	public Tenis() {
		addKeyListener(new KeyListener() {

			@Override
			public void keyTyped(final KeyEvent e) {
			}

			@Override
			public void keyReleased(final KeyEvent e) {
				racquet.keyReleased(e);
			}

			@Override
			public void keyPressed(final KeyEvent e) {
				racquet.keyPressed(e);
			}
		});
		setFocusable(true);
	}

	public int getScore() {
		return score;
	}

	public Racquet getRacquet() {
		return this.racquet;
	}

	public void plusSpeed(final int amount) {
		speed = speed + amount;
	}

	public void plusScore() {
		score++;
	}

	public int getSpeed() {
		return speed;
	}

	@Override
	protected void move() {
		ball.move();
		racquet.move();
	}

	@Override
	protected String getGameName() {
		return "Mini tenis";
	}

	@Override
	protected void paintThisGame(final Graphics2D g2d) {
		g2d.drawString(String.valueOf(getScore()), 10, 30);

		ball.paint(g2d);
		racquet.paint(g2d);
	}
}
