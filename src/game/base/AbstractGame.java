package game.base;

import game.Plataform;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public abstract class AbstractGame extends JPanel {

	private static final long serialVersionUID = 4239874548724992418L;
	private final int DEFAULT_WIDTH = 300;
	private final int DEFAULT_HEIGHT = 400;

	public static void main(final String[] args) throws InterruptedException {
		final JFrame frame = new JFrame();
		final AbstractGame game = new Plataform();
		frame.add(game);
		frame.setSize(game.getWidth(), game.getHeight());
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		while (true) {
			game.move();
			game.repaint();
			Thread.sleep(10);
		}
	}

	@Override
	public void paint(final Graphics g) {
		super.paint(g);
		super.paint(g);
		final Graphics2D g2d = (Graphics2D)g;
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

		g2d.setColor(Color.GRAY);
		g2d.setFont(new Font("Verdana", Font.BOLD, 30));

		paintThisGame(g2d);
	}

	public void gameOver() {
		JOptionPane.showMessageDialog(this, "Game Over", "Game Over", JOptionPane.YES_NO_OPTION);
		System.exit(ABORT);
	}

	@Override
	public int getWidth() {
		return DEFAULT_WIDTH;
	}

	@Override
	public int getHeight() {
		return DEFAULT_HEIGHT;
	}

	abstract protected void paintThisGame(final Graphics2D g2d);

	abstract protected void move();

	abstract protected String getGameName();

}
