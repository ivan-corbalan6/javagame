package game;

import game.base.AbstractGame;

import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import sprite.Square;
import sprite.base.Sprite;


public class Plataform extends AbstractGame {

	private static final long serialVersionUID = -2804985514537394945L;
	private final Square square = new Square(this, 0, 0);

	public Plataform() {
		sprites.add(square);
		generarMuro();
		addKeyListener(new KeyListener() {

			@Override
			public void keyTyped(final KeyEvent e) {
			}

			@Override
			public void keyReleased(final KeyEvent e) {
				square.keyReleased(e);
			}

			@Override
			public void keyPressed(final KeyEvent e) {
				square.keyPressed(e);
			}
		});
		setFocusable(true);
	}

	@Override
	protected String getGameName() {
		return "Mini tenis";
	}

	@Override
	protected void paintThisGame(final Graphics2D g2d) {
		for (final Sprite sprite : sprites) {
			sprite.paint(g2d);
		}
	}

	@Override
	public int getWidth() {
		return 800;
	}

	@Override
	public int getHeight() {
		return 600;
	}

	private void generarMuro() {
		for (int i = 0; i <= getHeight(); i++) {
			sprites.add(new Square(this, 500, i));
		}
	}
}
