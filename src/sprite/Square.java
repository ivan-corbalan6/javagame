package sprite;

import game.base.AbstractGame;

import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;

import sprite.base.Sprite;

public class Square extends Sprite {

	private final int WIDTH = 10;
	private final int HEIGTH = 10;

	private int x = 0;
	private int y = 0;
	private int xa = 0;
	private int ya = 0;

	public Square(final AbstractGame game) {
		super(game);
	}

	@Override
	public void move() {
		if (x + xa > 0 && x + xa < game.getWidth() - 60) {
			x = x + xa;
		}

		if (y + ya > 0 && y + ya < game.getHeight() - 60) {
			y = y + ya;
		}
	}

	@Override
	public void paint(final Graphics2D g) {
		g.fillRect(x, y , getWidth(), getHeight());
	}

	@Override
	public Rectangle getBounds() {
		return new Rectangle(x, y, getWidth(), getHeight());
	}

	@Override
	protected int getWidth() {
		return WIDTH;
	}

	@Override
	protected int getHeight() {
		return HEIGTH;
	}

	@Override
	public void keyReleased(final KeyEvent e) {
		switch (e.getKeyCode()) {
			case KeyEvent.VK_LEFT:
			case KeyEvent.VK_RIGHT:
				xa = 0;
				break;

			case KeyEvent.VK_UP:
			case KeyEvent.VK_DOWN:
				ya = 0;
				break;

			default:
				break;
		}
	}

	@Override
	public void keyPressed(final KeyEvent e) {
		switch (e.getKeyCode()) {
			case KeyEvent.VK_LEFT:
				xa = -3;
				break;

			case KeyEvent.VK_RIGHT:
				xa = 3;
				break;

			case KeyEvent.VK_UP:
				ya = -3;
				break;

			case KeyEvent.VK_DOWN:
				ya = 3;
				break;

			default:
				break;
		}
	}

}
