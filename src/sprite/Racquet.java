package sprite;

import game.base.AbstractGame;

import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;

import sprite.base.Sprite;

public class Racquet extends Sprite {

	private static final int Y = 330;
	private final int WIDTH = 60;
	private final int HEIGTH = 10;

	private int x = 30;
	private int xa = 0;

	public Racquet(final AbstractGame game) {
		super(game);
	}

	@Override
	public void move() {
		if (x + xa > 0 && x + xa < game.getWidth() - 60) {
			x = x + xa;
		}
	}

	@Override
	public void paint(final Graphics2D g) {
		g.fillRect(x, Y , getWidth(), getHeight());
	}

	@Override
	public Rectangle getBounds() {
		return new Rectangle(x, Y, getWidth(), getHeight());
	}

	public int getTopY() {
		return Y - getHeight();
	}

	@Override
	protected int getWidth() {
		return WIDTH;
	}

	@Override
	protected int getHeight() {
		return HEIGTH;
	}

	@Override
	public void keyReleased(final KeyEvent e) {
		xa = 0;
	}

	@Override
	public void keyPressed(final KeyEvent e) {
		switch (e.getKeyCode()) {
			case KeyEvent.VK_LEFT:
				xa = -3;
				break;

			case KeyEvent.VK_RIGHT:
				xa = 3;
				break;

			default:
				break;
		}
	}

}
