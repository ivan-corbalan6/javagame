package sprite;

import game.Tenis;

import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;

import sprite.base.Sprite;

public class Ball extends Sprite {

	private static final int DIAMETER = 30;

	private int x;
	private int y;
	private int xa = 1;
	private int ya = 1;

	private final Tenis tenisGame;

	public Ball(final Tenis game, final int x, final int y) {
		super(game);
		this.tenisGame = game;
		this.x = x;
		this.y = y;
	}

	@Override
	public void move() {
		if (y + ya > game.getHeight() - getHeight()) {
			game.gameOver();
		}

		if (x + xa < 0) {
			xa = tenisGame.getSpeed();
		} else if (x + xa > game.getWidth() - getHeight()) {
			xa = -tenisGame.getSpeed();
		}

		if (y + ya < 0) {
			ya = tenisGame.getSpeed();
		} else if (y + ya > game.getHeight() - getHeight()) {
			ya = -tenisGame.getSpeed();
		}

		if (collision()) {
			ya = -tenisGame.getSpeed();
			y = tenisGame.getRacquet().getTopY() - getHeight();
			tenisGame.plusScore();
			if (tenisGame.getScore() % 2 == 0 ) {
				tenisGame.plusSpeed(1);
			}
		}

		x = x + xa;
		y = y + ya;
	}

	@Override
	public void paint(final Graphics2D g) {
		g.fillOval(x, y, getHeight(), getHeight());
	}

	@Override
	public Rectangle getBounds() {
		return new Rectangle(x, y, getHeight(), getHeight());
	}

	private boolean collision() {
		return tenisGame.getRacquet().getBounds().intersects(getBounds());
	}

	@Override
	protected int getWidth() {
		return DIAMETER;
	}

	@Override
	protected int getHeight() {
		return DIAMETER;
	}

	@Override
	public void keyReleased(final KeyEvent e) {}

	@Override
	public void keyPressed(final KeyEvent e) {}
}
