package sprite.base;

import game.base.AbstractGame;

import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;

public abstract class Sprite {

	protected AbstractGame game;

	public Sprite(final AbstractGame game) {
		this.game = game;
	}

	abstract protected int getWidth();
	abstract protected int getHeight();
	abstract public void move();
	abstract public void paint(final Graphics2D g);
	abstract public Rectangle getBounds();

	abstract public void keyReleased(final KeyEvent e);
	abstract public void keyPressed(final KeyEvent e);

}
